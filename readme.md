# Model classes
This package contain all classes using in eSpace platform. 

## For Build package
1. Install TypeScript global
```cmd
    npm install -g typescript
```
2. Copy generated files from JSON schema to `src` folder

3. Compile sources
```cmd
npm start
```

## Uses library
1. Add this repositary to our project dependes and install `npm i`
```json
  "dependencies": {
    ...,
    "ep_lib_models": "git@gitlab.com:EsportFamily/ep_lib_models.git"
  },
```

2. Uses in code
```typescript
import { Classes, Interfaces, Consts } from 'ep_lib_model';

let c = new Classes.Bookmaker();
Object.assign(c, {id: 110, title: "1xbet"})
console.log('Test class', c);

let e = Consts.BetStatusEnum;
console.log('Test enum', e);

const _json = `
{
    "entity": "series",
    "entityId": 999,
    "status": "${e.open}",
    "bookmaker": {
        "id": 100,
        "title": "1xbet"
    }
}
`
let i = JSON.parse(_json) as Interfaces.IBet;
console.log('Test interface', i);
``` 