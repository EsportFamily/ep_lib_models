export declare enum AwardTypeEnum {
    mvp = "mvp"
}
export declare enum BetStatusEnum {
    close = "close",
    open = "open"
}
export declare enum BracketTypeEnum {
    Final = "Final",
    Lower = "Lower",
    Upper = "Upper",
    none = "none"
}
export declare enum CofTypeEnum {
    standoff = "standoff",
    win = "win"
}
export declare enum DOTAFactionsEnum {
    Dire = "Dire",
    Neutral = "Neutral",
    Radiant = "Radiant"
}
export declare enum DOTAMatchDelaysEnum {
    delay10 = "delay10",
    delay120 = "delay120",
    delay300 = "delay300"
}
export declare enum DOTAMatchSeriesTypesEnum {
    bo1 = "bo1",
    bo2 = "bo2",
    bo3 = "bo3",
    bo5 = "bo5",
    invalid = "invalid"
}
export declare enum DOTAMatchStatusesEnum {
    battle = "battle",
    draft = "draft",
    ended = "ended",
    inGame = "inGame",
    preStart = "preStart"
}
export declare enum DOTAPlayerPositionsEnum {
    Ease_line = "Ease line",
    Hard_line = "Hard line",
    Jungle = "Jungle",
    Mid = "Mid",
    Roaming = "Roaming"
}
export declare enum DOTAPlayerRolesEnum {
    Hard_Carry = "Hard Carry",
    Hard_Support = "Hard Support",
    Jungler = "Jungler",
    Mid = "Mid",
    Offlaner = "Offlaner"
}
export declare enum GameCodeEnum {
    csgo = "csgo",
    dota2 = "dota2",
    lol = "lol",
    underlords = "underlords"
}
export declare enum GameTypeEnum {
    moba = "moba",
    shooter = "shooter"
}
export declare enum HeroAspectsEnum {
    Agility = "Agility",
    Intelligence = "Intelligence",
    Strength = "Strength"
}
export declare enum HeroRoleEnum {
    role1 = "role1"
}
export declare enum ISOLangEnum {
    en = "en",
    ru = "ru",
    uk = "uk",
    zh = "zh"
}
export declare enum MapUnitTypeEnum {
    type1 = "type1"
}
export declare enum MatchStatusEnum {
    live = "live",
    past = "past",
    unknown = "unknown",
    upcoming = "upcoming"
}
export declare enum MediaItemKindEnum {
    background = "background",
    banner = "banner",
    columnBanner = "columnBanner",
    default = "default",
    large = "large",
    medium = "medium",
    screenshot = "screenshot",
    small = "small"
}
export declare enum MediaItemTypeEnum {
    image = "image",
    video = "video"
}
export declare enum MembershipTypeEnum {
    coach = "coach",
    manager = "manager",
    player = "player",
    stand_in = "stand_in"
}
export declare enum RegionEnum {
    CIS = "CIS",
    China = "China",
    Europe = "Europe",
    NorthAmerica = "NorthAmerica",
    SouthAmerica = "SouthAmerica",
    SoutheastAsia = "SoutheastAsia",
    unset = "unset"
}
export declare enum SeriesStatusEnum {
    live = "live",
    past = "past",
    unknown = "unknown",
    upcoming = "upcoming"
}
export declare enum StageShemaTypeEnum {
    DoubleElimination = "DoubleElimination",
    DoubleEliminationSeedLoser = "DoubleEliminationSeedLoser",
    GSLBrackets = "GSLBrackets",
    RoundRobin = "RoundRobin",
    ShowMatch = "ShowMatch",
    SingleElimination = "SingleElimination",
    Swiss = "Swiss",
    none = "none"
}
export declare enum StageStatusEnum {
    live = "live",
    past = "past",
    unknown = "unknown",
    upcoming = "upcoming"
}
export declare enum StatSeriesTypeEnum {
    gpm = "gpm",
    netWorth = "netWorth",
    xpm = "xpm"
}
export declare enum StreamPlatformEnum {
    twitch = "twitch",
    youtube = "youtube"
}
export declare enum StreamStatusEnum {
    active = "active",
    ended = "ended"
}
export declare enum TierTypeEnum {
    tier1 = "tier1",
    tier2 = "tier2",
    tier3 = "tier3"
}
export declare enum TournamentStatusEnum {
    live = "live",
    past = "past",
    unknown = "unknown",
    upcoming = "upcoming"
}
export declare enum UltimateStateEnum {
    no_mana = "no_mana",
    not_learned = "not_learned",
    ready = "ready",
    used = "used"
}
export declare enum WebLinkTypeEnum {
    facebook = "facebook",
    instagram = "instagram",
    twitch = "twitch",
    twitter = "twitter",
    youtube = "youtube"
}
export declare enum WinnerTypeEnum {
    none = "none",
    winner = "winner",
    winnerLower = "winnerLower"
}
export declare enum ModelClassType {
    Achivment = "Achivment",
    Award = "Award",
    Bet = "Bet",
    Bookmaker = "Bookmaker",
    BroadcastChannel = "BroadcastChannel",
    Caster = "Caster",
    CofData = "CofData",
    Competitior = "Competitior",
    Country = "Country",
    DOTAFaction = "DOTAFaction",
    DOTAHeroAspect = "DOTAHeroAspect",
    DOTAMatch = "DOTAMatch",
    DOTAMatchDraft = "DOTAMatchDraft",
    DOTAMatchPlayer = "DOTAMatchPlayer",
    DOTAMatchStatus = "DOTAMatchStatus",
    DOTAMatchTeam = "DOTAMatchTeam",
    DOTAMatchTeamMember = "DOTAMatchTeamMember",
    DOTAPlayer = "DOTAPlayer",
    DOTATeam = "DOTATeam",
    Game = "Game",
    Hero = "Hero",
    HeroAbility = "HeroAbility",
    HeroAbilityItemCompatibility = "HeroAbilityItemCompatibility",
    HeroRole = "HeroRole",
    HeroSkillBuild = "HeroSkillBuild",
    Item = "Item",
    ItemRecipe = "ItemRecipe",
    Location = "Location",
    MapUnit = "MapUnit",
    MapUnitState = "MapUnitState",
    Match = "Match",
    MatchScore = "MatchScore",
    MediaItem = "MediaItem",
    Organizer = "Organizer",
    Pair = "Pair",
    Person = "Person",
    PlayeItem = "PlayeItem",
    Player = "Player",
    PlayerPosition = "PlayerPosition",
    PlayerRole = "PlayerRole",
    PrizePlace = "PrizePlace",
    PrizePool = "PrizePool",
    Region = "Region",
    Roster = "Roster",
    Round = "Round",
    Score = "Score",
    Series = "Series",
    Stage = "Stage",
    Stream = "Stream",
    StreamPlatform = "StreamPlatform",
    StreamStatus = "StreamStatus",
    Team = "Team",
    TeamOwner = "TeamOwner",
    Teammate = "Teammate",
    Tier = "Tier",
    TimeSeries = "TimeSeries",
    Tournament = "Tournament",
    Translation = "Translation",
    TranslationText = "TranslationText",
    WebLink = "WebLink"
}
