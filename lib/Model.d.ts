import * as consts from './Model.consts';
import * as interfaces from './Model.interfaces';
export declare class AbstractClass {
}
export declare class Achivment extends AbstractClass implements interfaces.IAchivment {
    $$class: string;
    place1st: number;
    place2nd: number;
    place3rd: number;
}
export declare class Award extends AbstractClass implements interfaces.IAward {
    $$class: string;
    player: Player;
    type: consts.AwardTypeEnum;
}
export declare class Bet extends AbstractClass implements interfaces.IBet {
    $$class: string;
    bookmaker: Bookmaker;
    cofs: CofData[];
    entity: string;
    entityId: number;
    originalId: string;
    status: consts.BetStatusEnum;
}
export declare class Bookmaker extends AbstractClass implements interfaces.IBookmaker {
    $$class: string;
    affiliateData: string;
    homeUrl: string;
    id: string;
    media: MediaItem[];
    order: number;
    title: string;
}
export declare class BroadcastChannel extends AbstractClass implements interfaces.IBroadcastChannel {
    $$class: string;
    langue: consts.ISOLangEnum;
    name: string;
}
export declare class Caster extends AbstractClass implements interfaces.ICaster {
    $$class: string;
    channel: BroadcastChannel;
    name: string;
    player: DOTAPlayer;
}
export declare class CofData extends AbstractClass implements interfaces.ICofData {
    $$class: string;
    cof: number;
    cofParam: string;
    cofType: consts.CofTypeEnum;
}
export declare class Competitior extends AbstractClass implements interfaces.ICompetitior {
    $$class: string;
    roster: Roster;
    winType: consts.WinnerTypeEnum;
}
export declare class Country extends AbstractClass implements interfaces.ICountry {
    $$class: string;
    iso: string;
    media: MediaItem[];
    title: Translation;
}
export declare class DOTAFaction extends AbstractClass implements interfaces.IDOTAFaction {
    $$class: string;
    id: consts.DOTAFactionsEnum;
    name: Translation;
}
export declare class DOTAHeroAspect extends AbstractClass implements interfaces.IDOTAHeroAspect {
    $$class: string;
    id: consts.HeroAspectsEnum;
    name: Translation;
}
export declare class DOTAMatch extends AbstractClass implements interfaces.IDOTAMatch {
    $$class: string;
    broadcasters: Caster[];
    broadcastsChannels: BroadcastChannel[];
    createdDate: Date;
    delay: consts.DOTAMatchDelaysEnum;
    dotaId: number;
    drafts: DOTAMatchDraft[];
    firstBloodTime: number;
    id: string;
    mapUnits: MapUnitState[];
    match: Match;
    patch: string;
    radiantWin: boolean;
    seriesType: consts.DOTAMatchSeriesTypesEnum;
    spectatorsCount: number;
    status: DOTAMatchStatus;
    teams: DOTAMatchTeam[];
    time: number;
}
export declare class DOTAMatchDraft extends AbstractClass implements interfaces.IDOTAMatchDraft {
    $$class: string;
    faction: DOTAFaction;
    heroe: Hero;
    isPick: boolean;
    order: number;
}
export declare class DOTAMatchPlayer extends AbstractClass implements interfaces.IDOTAMatchPlayer {
    $$class: string;
    assist: number;
    builds: HeroSkillBuild[];
    denied: number;
    detch: number;
    gold: number;
    gpm: number;
    hero: Hero;
    id: string;
    items: PlayeItem[];
    kills: number;
    lastHit: number;
    level: number;
    name: string;
    netWorth: number;
    player: DOTAPlayer;
    position: PlayerPosition;
    positionX: number;
    positionY: number;
    respawnTimer: number;
    role: PlayerRole;
    slotNumber: number;
    ultimateCooldown: number;
    ultimateState: consts.UltimateStateEnum;
    xpm: number;
}
export declare class DOTAMatchStatus extends AbstractClass implements interfaces.IDOTAMatchStatus {
    $$class: string;
    id: consts.DOTAMatchStatusesEnum;
    name: Translation;
}
export declare class DOTAMatchTeam extends AbstractClass implements interfaces.IDOTAMatchTeam {
    $$class: string;
    faction: DOTAFaction;
    gpm: number;
    gpmStat: TimeSeries[];
    name: string;
    netWorth: number;
    netWorthStat: TimeSeries[];
    players: DOTAMatchTeamMember[];
    score: number;
    team: DOTATeam;
    xpm: number;
    xpmStat: TimeSeries[];
}
export declare class DOTAMatchTeamMember extends AbstractClass implements interfaces.IDOTAMatchTeamMember {
    $$class: string;
    id: string;
    name: string;
    player: DOTAPlayer;
    slotNumber: number;
}
export declare class DOTAPlayer extends AbstractClass implements interfaces.IDOTAPlayer {
    $$class: string;
    dotaId: number;
    featuredHeroes: Hero[];
    id: string;
    player: Player;
    position: PlayerPosition;
    role: PlayerRole;
}
export declare class DOTATeam extends AbstractClass implements interfaces.IDOTATeam {
    $$class: string;
    DPCpoints: number;
    devision: string;
    dotaId: number;
    id: string;
    team: Team;
}
export declare class Game extends AbstractClass implements interfaces.IGame {
    $$class: string;
    code: consts.GameCodeEnum;
    media: MediaItem[];
    name: Translation;
    type: consts.GameTypeEnum;
}
export declare class Hero extends AbstractClass implements interfaces.IHero {
    $$class: string;
    abilities: HeroAbility[];
    aspect: DOTAHeroAspect;
    dotaId: number;
    faction: DOTAFaction;
    id: string;
    lore: string;
    media: MediaItem[];
    name: string;
    patch: string;
    role: HeroRole[];
    specifications: Pair[];
}
export declare class HeroAbility extends AbstractClass implements interfaces.IHeroAbility {
    $$class: string;
    dotaId: string;
    effects: Pair[];
    id: number;
    itemsCompatibility: Pair[];
    media: MediaItem[];
    name: string;
    patch: string;
    specifications: Pair[];
}
export declare class HeroAbilityItemCompatibility extends AbstractClass implements interfaces.IHeroAbilityItemCompatibility {
    $$class: string;
    ability: HeroAbility;
    item: Item;
}
export declare class HeroRole extends AbstractClass implements interfaces.IHeroRole {
    $$class: string;
    id: consts.HeroRoleEnum;
    name: Translation;
}
export declare class HeroSkillBuild extends AbstractClass implements interfaces.IHeroSkillBuild {
    $$class: string;
    ability: HeroAbility;
    abilityLevel: number;
    level: number;
    order: number;
}
export declare class Item extends AbstractClass implements interfaces.IItem {
    $$class: string;
    cost: number;
    dotaId: number;
    id: string;
    lore: string;
    media: MediaItem[];
    name: string;
    patch: string;
    specifications: Pair[];
}
export declare class ItemRecipe extends AbstractClass implements interfaces.IItemRecipe {
    $$class: string;
    cost: number;
    dotaId: number;
    id: string;
    isRecipe: boolean;
    lore: string;
    media: MediaItem[];
    name: string;
    patch: string;
    recipRequirements: Item[];
    recipeResult: Item;
    specifications: Pair[];
}
export declare class Location extends AbstractClass implements interfaces.ILocation {
    $$class: string;
    country: Country;
    location: Translation;
    region: Region;
}
export declare class MapUnit extends AbstractClass implements interfaces.IMapUnit {
    $$class: string;
    dotaId: string;
    faction: DOTAFaction;
    id: string;
    name: string;
    patch: string;
    positionX: number;
    positionY: number;
    type: consts.MapUnitTypeEnum;
}
export declare class MapUnitState extends AbstractClass implements interfaces.IMapUnitState {
    $$class: string;
    destroyed: boolean;
    destroyedTime: number;
    respawnTimer: number;
}
export declare class Match extends AbstractClass implements interfaces.IMatch {
    $$class: string;
    DOTAMatch: DOTAMatch;
    end: Date;
    game: consts.GameCodeEnum;
    id: number;
    scoreboard: MatchScore[];
    series: Series;
    start: Date;
    status: consts.MatchStatusEnum;
    streams: Stream[];
}
export declare class MatchScore extends AbstractClass implements interfaces.IMatchScore {
    $$class: string;
    order: number;
    roster: Roster;
    score: number;
}
export declare class MediaItem extends AbstractClass implements interfaces.IMediaItem {
    $$class: string;
    entity: string;
    entityId: number;
    kind: consts.MediaItemKindEnum;
    type: consts.MediaItemTypeEnum;
    url: string;
}
export declare class Organizer extends AbstractClass implements interfaces.IOrganizer {
    $$class: string;
    description: Translation;
    id: number;
    media: MediaItem[];
    title: string;
}
export declare class Pair extends AbstractClass implements interfaces.IPair {
    $$class: string;
    key: string;
    value: string;
}
export declare class Person extends AbstractClass implements interfaces.IPerson {
    $$class: string;
    birtday: Date;
    country: Country;
    description: Translation;
    earnings: number;
    id: number;
    internationalName: string;
    links: WebLink[];
    media: MediaItem[];
    name: string;
    players: Player[];
}
export declare class PlayeItem extends AbstractClass implements interfaces.IPlayeItem {
    $$class: string;
    item: Item;
    slotNumber: number;
    time: number;
}
export declare class Player extends AbstractClass implements interfaces.IPlayer {
    $$class: string;
    DOTAPlayer: DOTAPlayer;
    achivments: Achivment;
    active: boolean;
    alternativeNicks: string;
    game: Game;
    id: number;
    media: MediaItem[];
    nick: string;
    person: Person;
    teammate: Teammate;
}
export declare class PlayerPosition extends AbstractClass implements interfaces.IPlayerPosition {
    $$class: string;
    id: consts.DOTAPlayerPositionsEnum;
    name: Translation;
}
export declare class PlayerRole extends AbstractClass implements interfaces.IPlayerRole {
    $$class: string;
    id: consts.DOTAPlayerRolesEnum;
    name: Translation;
}
export declare class PrizePlace extends AbstractClass implements interfaces.IPrizePlace {
    $$class: string;
    place: number;
    points: number;
    prize: number;
    series: Series;
    team: Team;
}
export declare class PrizePool extends AbstractClass implements interfaces.IPrizePool {
    $$class: string;
    description: Translation;
    places: PrizePlace[];
    points: number;
    prizeTotal: number;
}
export declare class Region extends AbstractClass implements interfaces.IRegion {
    $$class: string;
    id: consts.RegionEnum;
    title: Translation;
}
export declare class Roster extends AbstractClass implements interfaces.IRoster {
    $$class: string;
    created: Date;
    id: number;
    members: Teammate[];
    team: Team;
}
export declare class Round extends AbstractClass implements interfaces.IRound {
    $$class: string;
    bestOf: number;
    bracket: consts.BracketTypeEnum;
    num: number;
    series: Series[];
    title: Translation;
}
export declare class Score extends AbstractClass implements interfaces.IScore {
    $$class: string;
    order: number;
    roster: Roster;
    score: number;
    turnamentPoints: number;
    winner: boolean;
}
export declare class Series extends AbstractClass implements interfaces.ISeries {
    $$class: string;
    bestOf: number;
    bets: Bet[];
    end: Date;
    game: consts.GameCodeEnum;
    gridRow: number;
    id: number;
    losToSeriesId: string;
    matches: Match[];
    place: PrizePlace;
    round: Round;
    scheduled: Date;
    scoreboard: Score[];
    start: Date;
    status: consts.SeriesStatusEnum;
    tournament: Tournament;
    viewersCount: number;
    viewersMaxCount: number;
    winToSeriesId: boolean;
}
export declare class Stage extends AbstractClass implements interfaces.IStage {
    $$class: string;
    bestOf: number;
    competitors: Competitior[];
    end: Date;
    exitLowerTeams: number;
    exitTeams: number;
    id: number;
    location: Location;
    nextStage: Stage;
    offline: boolean;
    place3rd: boolean;
    rounds: Round[];
    rules: Translation;
    schema: consts.StageShemaTypeEnum;
    start: Date;
    status: consts.StageStatusEnum;
    sub: Stage[];
    title: Translation;
}
export declare class Stream extends AbstractClass implements interfaces.IStream {
    $$class: string;
    Match: Match;
    delay: number;
    id: string;
    language: string;
    media: MediaItem[];
    name: string;
    ownId: string;
    platform: StreamPlatform;
    status: StreamStatus;
    title: string;
    viewersCount: number;
}
export declare class StreamPlatform extends AbstractClass implements interfaces.IStreamPlatform {
    $$class: string;
    Stream: Stream[];
    id: consts.StreamPlatformEnum;
    media: MediaItem[];
    title: Translation;
}
export declare class StreamStatus extends AbstractClass implements interfaces.IStreamStatus {
    $$class: string;
    Stream: Stream[];
    id: consts.StreamStatusEnum;
    title: Translation;
}
export declare class Team extends AbstractClass implements interfaces.ITeam {
    $$class: string;
    DOTATeam: DOTATeam;
    achivments: Achivment;
    alternativeNames: string;
    country: Country;
    description: Translation;
    earnings: number;
    establish: Date;
    game: consts.GameCodeEnum;
    id: number;
    internationalName: string;
    media: MediaItem[];
    members: Teammate[];
    name: string;
    owner: TeamOwner;
    pro: boolean;
    shortName: string;
}
export declare class TeamOwner extends AbstractClass implements interfaces.ITeamOwner {
    $$class: string;
    description: Translation;
    id: number;
    media: MediaItem[];
    title: string;
}
export declare class Teammate extends AbstractClass implements interfaces.ITeammate {
    $$class: string;
    join: Date;
    leave: Date;
    membership: consts.MembershipTypeEnum;
    player: Player;
    team: Team;
}
export declare class Tier extends AbstractClass implements interfaces.ITier {
    $$class: string;
    name: Translation;
    type: consts.TierTypeEnum;
}
export declare class TimeSeries extends AbstractClass implements interfaces.ITimeSeries {
    $$class: string;
    time: number;
    value: number;
}
export declare class Tournament extends AbstractClass implements interfaces.ITournament {
    $$class: string;
    awards: Award[];
    description: Translation;
    end: Date;
    game: Game;
    gameVersion: string;
    hidden: boolean;
    homeUrl: string;
    id: number;
    isPoints: boolean;
    location: Location;
    media: MediaItem[];
    offline: boolean;
    organizer: Organizer;
    prizePool: PrizePool;
    rosters: Roster[];
    rules: Translation;
    series: Series[];
    stages: Stage[];
    start: Date;
    status: consts.TournamentStatusEnum;
    teamsCount: number;
    tier: Tier;
    title: Translation;
}
export declare class Translation extends AbstractClass implements interfaces.ITranslation {
    $$class: string;
    en: TranslationText;
    ru: TranslationText;
    ua: TranslationText;
    zh: TranslationText;
}
export declare class TranslationText extends AbstractClass implements interfaces.ITranslationText {
    $$class: string;
    freez: boolean;
    lastUser: boolean;
    plain: boolean;
    revision: number;
    text: string;
}
export declare class WebLink extends AbstractClass implements interfaces.IWebLink {
    $$class: string;
    alt: Translation;
    type: consts.WebLinkTypeEnum;
    url: string;
}
